#include <WiFi.h>

const char* ssid = "HUAWEI-m5WH";
const char* password = "mjHDqt4M";
const int serverPort = 80;


WiFiServer server(serverPort);

float receivedTemperature = 0.0;
float receivedHumidity = 0.0;
float receivedGas = 0.0;

bool isAuthenticated = false;
String username = "admin";
String pss = "password";

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi");
  Serial.println(WiFi.localIP());

  server.begin();
}

void loop() {
  
  WiFiClient client = server.available();
  if (client) {
    Serial.println("Client conectat");

    String line = client.readStringUntil('\n');
    if (line.length() > 0) {
      int firstComma = line.indexOf(',');
      int secondComma = line.indexOf(',', firstComma + 1);

      if (firstComma != -1 && secondComma != -1) {
        receivedTemperature = line.substring(0, firstComma).toFloat();
        receivedHumidity = line.substring(firstComma + 1, secondComma).toFloat();
        receivedGas = line.substring(secondComma + 1).toFloat();
        Serial.println("Date trimise cu succes");
      } else {
        Serial.println("Datele nu au fost trimise");
      }
    }

    
/*
    client.println("HTTP/1.1 200 OK");
    client.println("Content-type:text/html");
    client.println();
    client.println("<!DOCTYPE html><html><head><title>ESP32 Sensor Data</title></head><body>");
    client.println("<h1>Sensor Data</h1>");
    client.print("<p>Temperature: ");
    client.print(receivedTemperature);
    client.print(" &deg;C</p>");
    client.print("<p>Humidity: ");
    client.print(receivedHumidity);
    client.print(" %</p>");
    client.print("<p>Gas: ");
    client.print(receivedGas);
    client.print(" ppm</p>");
    client.println("</body></html>");

    Serial.println("Date primite:");
    Serial.println(receivedTemperature);
    Serial.println(receivedHumidity);
    Serial.println(receivedGas);
*/
String loginPage = "HTTP/1.1 200 OK\r\n"
                   "Content-type:text/html\r\n"
                   "\r\n"
                   "<!DOCTYPE html><html><head><title>Log IN page</title>"
                   "<style>"
                   "body { display: flex; justify-content: center; align-items: center; height: 100vh; margin: 0; font-family: Arial, sans-serif; background-color: #f0f0f0; }"
                   ".login-container { background-color: #ffffff; padding: 20px; border-radius: 8px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); text-align: center; }"
                   ".login-container h2 { margin-bottom: 20px; color: #333333; }"
                   ".login-container input[type=\"text\"], .login-container input[type=\"password\"] { width: 100%; padding: 10px; margin: 10px 0; border: 1px solid #cccccc; border-radius: 4px; }"
                   ".login-container input[type=\"submit\"] { width: 100%; padding: 10px; border: none; border-radius: 4px; background-color: #4CAF50; color: white; font-size: 16px; }"
                   ".login-container input[type=\"submit\"]:hover { background-color: #45a049; }"
                   "</style>"
                   "</head><body>"
                   "<div class=\"login-container\">"
                   "<h2>Login</h2>"
                   "<form action=\"/login\" method=\"post\">"
                   "<input type=\"text\" name=\"username\" placeholder=\"Username\" required><br>"
                   "<input type=\"password\" name=\"pss\" placeholder=\"Password\" required><br>"
                   "<input type=\"submit\" value=\"Login\">"
                   "</form>"
                   "</div>"
                   "</body></html>";

String mainpage = "HTTP/1.1 200 OK\r\n"
                     "Content-type:text/html\r\n"
                     "\r\n"
                     "<!DOCTYPE html><html><head><title>ESP32 Sensor Data</title>"
                     "<style>"
                     "body { text-align: center; }"
                     "h1 { color: red; margin-top: 50px; }"
                     "p { color: green; margin: 20px 0;}"
                     "</style>"
                     "</head><body>"
                     "<h1>Sensor Data</h1>"
                     "<p>Temperature: " + String(receivedTemperature) + " &deg;C</p>"
                     "<p>Humidity: " + String(receivedHumidity) + " %</p>"
                     "<p>Gas: " + String(receivedGas) + " ppm</p>"
                     "</body></html>";


//client.print(loginPage);
//delay(10000);
client.print(mainpage);

Serial.println("Date primite:");
Serial.println(receivedTemperature);
Serial.println(receivedHumidity);
Serial.println(receivedGas);


    client.stop();
    Serial.println("Client disconnected");
  }
}